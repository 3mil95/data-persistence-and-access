﻿using Microsoft.Data.SqlClient;
using ChinookDb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChinookDb.DataAccess
{
    public class CustomerRepository : ICustomerRepository
    {
        public List<Customer> GetAll()
        {
            List<Customer> customers = new List<Customer>();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";

            try
            {
                // Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                // Handle results
                                Customer temp = new Customer();
                                temp.CustomerID = reader.GetInt32(0);
                                temp.FirstName = !reader.IsDBNull(1) ? reader.GetString(1) : "";
                                temp.LastName = !reader.IsDBNull(2) ? reader.GetString(2) : "";
                                temp.Country = !reader.IsDBNull(3) ? reader.GetString(3) : "";
                                temp.Phone = !reader.IsDBNull(4) ? reader.GetString(4) : "";
                                temp.Email = !reader.IsDBNull(5) ? reader.GetString(5) : "";

                                customers.Add(temp);
                            }
                        }
                    }
                }
            } catch(Exception ex)
            {
                Console.WriteLine(ex);
            }

            return customers;
        }

        public Customer Get(int id)
        {
            Customer customer = null;
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE CustomerId = @CustomerId";

            try
            {
                // Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        // Reader
                        cmd.Parameters.AddWithValue("@CustomerId", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // Handle results
                            if (reader.Read())
                            {
                                customer = new Customer();
                                customer.CustomerID = reader.GetInt32(0);
                                customer.FirstName = !reader.IsDBNull(1) ? reader.GetString(1) : "";
                                customer.LastName = !reader.IsDBNull(2) ? reader.GetString(2) : "";
                                customer.Country = !reader.IsDBNull(3) ? reader.GetString(3) : "";
                                customer.Phone = !reader.IsDBNull(4) ? reader.GetString(4) : "";
                                customer.Email = !reader.IsDBNull(5) ? reader.GetString(5) : "";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return customer;
        }

        public Customer FindWithName(string name)
        {
            Customer customer = null;
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE FirstName + ' ' + LastName LIKE @CustomerName";

            try
            {
                // Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        // Reader
                        cmd.Parameters.AddWithValue("@CustomerName", name + "%");
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // Handle results
                            if (reader.Read())
                            {
                                customer = new Customer();
                                customer.CustomerID = reader.GetInt32(0);
                                customer.FirstName = !reader.IsDBNull(1) ? reader.GetString(1) : "";
                                customer.LastName = !reader.IsDBNull(2) ? reader.GetString(2) : "";
                                customer.Country = !reader.IsDBNull(3) ? reader.GetString(3) : "";
                                customer.Phone = !reader.IsDBNull(4) ? reader.GetString(4) : "";
                                customer.Email = !reader.IsDBNull(5) ? reader.GetString(5) : "";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return customer;
        }

        public List<Customer> GetPage(int offset, int limit)
        {
            List<Customer> customers = new List<Customer>();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer ORDER BY CustomerId OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY";

            try
            {
                // Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@Limit", limit);
                        cmd.Parameters.AddWithValue("@Offset", offset);

                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                // Handle results
                                Customer temp = new Customer();
                                temp.CustomerID = reader.GetInt32(0);
                                temp.FirstName = !reader.IsDBNull(1) ? reader.GetString(1) : "";
                                temp.LastName = !reader.IsDBNull(2) ? reader.GetString(2) : "";
                                temp.Country = !reader.IsDBNull(3) ? reader.GetString(3) : "";
                                temp.Phone = !reader.IsDBNull(4) ? reader.GetString(4) : "";
                                temp.Email = !reader.IsDBNull(5) ? reader.GetString(5) : "";

                                customers.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return customers;
        }

        public bool Add(Customer customer)
        {
            bool success = false;
            string sql = @"INSERT INTO Customer(FirstName, LastName, Country, PostalCode, Phone, Email) 
                           VALUES(@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";

            try
            {
                // Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);

                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            } catch(Exception ex)
            {
                Console.WriteLine(ex);
            }

            return success;
        }

        public bool Update(Customer customer)
        {
            bool success = false;
            string sql = @"UPDATE Customer
                           SET FirstName = @FirstName, LastName = @LastName, Country = @Country, PostalCode = @PostalCode, Phone = @Phone, Email = @Email
                           WHERE CustomerId = @CustomerId";

            try
            {
                // Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", customer.CustomerID);
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);

                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return success;
        }

        public List<CustomerCountry> GetNuberOfCustomerInCountries()
        {
            List<CustomerCountry> customerCountrys = new List<CustomerCountry>();
            string sql = "SELECT Country, COUNT(*)  FROM Customer GROUP BY Country ORDER BY COUNT(*) DESC";

            try
            {
                // Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                // Handle results
                                CustomerCountry temp = new CustomerCountry();
                                temp.Country = reader.GetString(0);
                                temp.NumberOfCustomers = reader.GetInt32(1);

                                customerCountrys.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return customerCountrys;
        }

        public List<CustomerSpender> GetHighestSpendingCustomers()
        {
            List<CustomerSpender> customers = new List<CustomerSpender>();
            string sql = @"SELECT Customer.CustomerId, FirstName, LastName, SUM(Total) FROM Customer 
                           LEFT JOIN Invoice ON ( Customer.CustomerId = Invoice.CustomerId ) 
                           GROUP BY Customer.CustomerId, FirstName, LastName
                           ORDER BY SUM(Total) DESC";

            try
            {
                // Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                // Handle results
                                CustomerSpender temp = new CustomerSpender();
                                temp.CustomerID = reader.GetInt32(0);
                                temp.FirstName = !reader.IsDBNull(1) ? reader.GetString(1) : "";
                                temp.LastName = !reader.IsDBNull(2) ? reader.GetString(2) : "";
                                temp.TotalSpending = !reader.IsDBNull(3) ? (float) reader.GetDecimal(3) : 0f;
                                customers.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return customers;
        }

        public CustomerGenre GetCustomerTopGenre(int customerId)
        {
            CustomerGenre customerGenre = null;
            
            string sql = @"SELECT TOP 1 WITH TIES Invoice.CustomerId, FirstName, LastName, Genre.Name FROM Customer
                            LEFT JOIN Invoice ON (Customer.CustomerId = Invoice.CustomerId)
                            LEFT JOIN InvoiceLine ON (Invoice.InvoiceId = InvoiceLine.InvoiceId) 
                            LEFT JOIN Track ON (InvoiceLine.TrackId = Track.TrackId) 
                            LEFT JOIN Genre ON (Track.GenreId = Genre.GenreId)
                            WHERE Invoice.CustomerId = @CustomerId
                            GROUP BY Invoice.CustomerId, FirstName, LastName, Genre.Name
                            ORDER BY COUNT(*) DESC";

            try
            {
                // Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", customerId);

                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {

                            // Handle results
                            customerGenre = new CustomerGenre();
                            if (reader.Read())
                            {
                                customerGenre.CustomerID = reader.GetInt32(0);
                                customerGenre.FirstName = !reader.IsDBNull(1) ? reader.GetString(1) : "";
                                customerGenre.LastName = !reader.IsDBNull(2) ? reader.GetString(2) : "";
                            }

                            List<string> topGenre = customerGenre.TopGenre;
                            do
                            {
                                if (!reader.IsDBNull(3))
                                {
                                    topGenre.Add(reader.GetString(3));
                                }
                            }
                            while (reader.Read());

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return customerGenre;
        }
    }
}
