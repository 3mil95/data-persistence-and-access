﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChinookDb.DataAccess
{
    public interface IRepository<T> 
    {
        /// <summary>
        /// Get all itmes in the repository.
        /// </summary>
        /// <returns>A list with all the items.</returns>
        public List<T> GetAll();

        /// <summary>
        /// Get a singel item in the reposetory by id.
        /// </summary>
        /// <param name="id">The id of the item that is returend.</param>
        /// <returns>A singel item or null.</returns>
        public T Get(int id);

        /// <summary>
        /// Add a item to the reposetory.
        /// </summary>
        /// <param name="item">The item to be added to the repository.</param>
        /// <returns>Retrns if the item is added or not</returns>
        public bool Add(T item);

        /// <summary>
        /// Updates a item in the reposotroy.
        /// </summary>
        /// <param name="item">The updated item</param>
        /// <returns>Retrns if the item is updated or not</returns>
        public bool Update(T item);
    }
}
