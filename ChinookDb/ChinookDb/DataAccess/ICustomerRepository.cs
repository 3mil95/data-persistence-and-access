﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChinookDb.Models;

namespace ChinookDb.DataAccess
{
    public interface ICustomerRepository : IRepository<Customer>
    {
        /// <summary>
        /// Find a customer by its name in the repository. 
        /// </summary>
        /// <param name="name">The name of the customer to be fund.</param>
        /// <returns>Returns the custemor found with the name or null.</returns>
        public Customer FindWithName(string name);

        /// <summary>
        /// Gets a page of customers from the repository.
        /// </summary>
        /// <param name="offset">The start custemer offset of the page.</param>
        /// <param name="limit">The number of customers on the page.</param>
        /// <returns>Returns a list containing the pages customers.</returns>
        public List<Customer> GetPage(int offset, int limit);

        /// <summary>
        /// Gets all countries and how many customers in that country, arranged in descending order by number of customers.
        /// </summary>
        /// <returns>Returns a list of CustomerCountry containing the country and the number of customers.</returns>
        public List<CustomerCountry> GetNuberOfCustomerInCountries();

        /// <summary>
        /// provides all customers and their total invoice, arranged in descending order by tottal invoice.
        /// </summary>
        /// <returns>Returns a list of CustomerSpender containing name, ID and total invoice</returns>
        public List<CustomerSpender> GetHighestSpendingCustomers();


        /// <summary>
        /// Provides top music genre or genres for a customer.
        /// </summary>
        /// <param name="customer">The customer.</param>
        /// <returns>Returns a CustomerGenre containing the info abute the customer and a list of the top genres or null.</returns>
        public CustomerGenre GetCustomerTopGenre(int id);
    }
}
