﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChinookDb.Models
{
    public class CustomerSpender
    {
        public int CustomerID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public float TotalSpending { get; set; }

        public override string ToString()
        {
            return $"Customer: {CustomerID} {FirstName} {LastName} Toatl: {TotalSpending}";
        }
    }
}
