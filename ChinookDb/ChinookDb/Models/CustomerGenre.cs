﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChinookDb.Models
{
    public class CustomerGenre
    {
        public int CustomerID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public List<String> TopGenre { get; private set; } = new List<String>();

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder($"{CustomerID} {FirstName} {LastName}");
            stringBuilder.Append(" [ ");
            foreach (string ganre in TopGenre)
            {
                stringBuilder.Append(ganre);
                stringBuilder.Append(", ");
            }

            stringBuilder.Append("]");

            return stringBuilder.ToString();
        }
    }
}
