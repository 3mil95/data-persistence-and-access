﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChinookDb.DataAccess;
using ChinookDb.Models;


namespace ChinookDb
{
    public class CustomerTerminal
    {

        private readonly ICustomerRepository repository;

        public CustomerTerminal(ICustomerRepository repository)
        {
            this.repository = repository;
        }

        public string GetHeader()
        {
            return @"CustomerDB:
1 - Get all customers
2 - Get customer by id
3 - Get customer by name
4 - Get a customer page
5 - Add a customer
6 - Update customer
7 - Get number of customers in cuntrys
8 - Get highest spenders
9 - Get customers top genre(s)";
        }

        public void Start()
        {
            bool runing = true;

            while (runing)
            {
                Console.Clear();
                Console.WriteLine(GetHeader());
                String commandNumber = Input("Select command: ");
                RunCommand(commandNumber);
                String newCommand = Input("New command (y/n): ");
                if (newCommand != "y")
                {
                    break;
                }
            }
        }


        private void RunCommand(string commandNumber)
        {
            switch (commandNumber)
            {
                case "1":
                    WriteList(repository.GetAll());
                    break;
                case "2":
                    int id = NumberInput("Id: ");
                    Console.WriteLine(repository.Get(id));
                    break;
                case "3":
                    string name = Input("Name: ");
                    Console.WriteLine(repository.FindWithName(name));
                    break;
                case "4":
                    int offset = NumberInput("Offset: ");
                    int limit = NumberInput("Limit: ");
                    WriteList(repository.GetPage(offset, limit));
                    break;
                case "5":
                    Customer newCustomer = new Customer();
                    newCustomer.FirstName = Input("First name: ");
                    newCustomer.LastName = Input("Last name: ");
                    newCustomer.Country = Input("Country: ");
                    newCustomer.PostalCode = Input("PostalCode: ");
                    newCustomer.Phone = Input("Phone: ");
                    newCustomer.Email = Input("Email: ");
                    Console.WriteLine(repository.Add(newCustomer) ? "Added 1" : "Added 0");
                    break;
                case "6":
                    Customer updatedCustomer = new Customer();
                    updatedCustomer.CustomerID = NumberInput("id: ");
                    updatedCustomer.FirstName = Input("First name: ");
                    updatedCustomer.LastName = Input("Last name: ");
                    updatedCustomer.Country = Input("Country: ");
                    updatedCustomer.PostalCode = Input("PostalCode: ");
                    updatedCustomer.Phone = Input("Phone: ");
                    updatedCustomer.Email = Input("Email: ");
                    Console.WriteLine(repository.Update(updatedCustomer) ? "Updated 1" : "Updated 0");
                    break;
                case "7":
                    WriteList(repository.GetNuberOfCustomerInCountries());
                    break;
                case "8":
                    WriteList(repository.GetHighestSpendingCustomers());
                    break;
                case "9":
                    id = NumberInput("Id: ");
                    Console.WriteLine(repository.GetCustomerTopGenre(id));
                    break;
                default:
                    Console.WriteLine("Not a command!");
                    break;

            }
        }


        private int NumberInput(string text)
        {
            while (true)
            {
                Console.Write(text);
                string input = Console.ReadLine();
                try
                {
                    return int.Parse(input);
                } catch {
                    Console.WriteLine("Not a int");
                }
            }
            
        }

        private string Input(string text)
        {
            Console.Write(text);
            return Console.ReadLine();
        }

        static void WriteList(IEnumerable<Object> objects)
        {
            foreach (Object obj in objects)
            {
                Console.WriteLine(obj);
            }
        }
    }
}
