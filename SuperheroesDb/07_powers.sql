USE SuperheroesDb;

INSERT INTO Heropower (power_name, power_description) VALUES ('Money', 'A lot of money');
INSERT INTO Heropower (power_name, power_description) VALUES ('Laser eyes', 'Shoots laser from their eyes'); 
INSERT INTO Heropower (power_name, power_description) VALUES ('Super strength', 'Can lift most things'); 

INSERT INTO Superhero_Power (superhero_id, power_id) VALUES (1, 1)
INSERT INTO Superhero_Power (superhero_id, power_id) VALUES (2, 2)
INSERT INTO Superhero_Power (superhero_id, power_id) VALUES (3, 2)
INSERT INTO Superhero_Power (superhero_id, power_id) VALUES (3, 3)
GO