USE SuperheroesDb;

CREATE TABLE Superhero (
    superhero_Id int IDENTITY PRIMARY KEY,
    superhero_name nvarchar(50) NOT NULL,
    superhero_alias nvarchar(50),
    superhero_origin nvarchar(500),
);

CREATE TABLE Assistant (
    assistant_id int IDENTITY PRIMARY KEY,
    assistant_name nvarchar(50) NOT NULL,
);

CREATE TABLE Heropower (
    power_id int IDENTITY PRIMARY KEY,
    power_name nvarchar(50) NOT NULL,
    power_description nvarchar(200),
);

