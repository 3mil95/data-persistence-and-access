USE master;
GO

DROP DATABASE SuperheroesDb;
GO

CREATE DATABASE SuperheroesDb;
GO

USE SuperheroesDb;

CREATE TABLE Superhero (
    superhero_Id int IDENTITY PRIMARY KEY,
    superhero_name varchar(50) NOT NULL,
    superhero_alias varchar(50),
    superhero_origin varchar(500),
);

CREATE TABLE Assistant (
    assistant_id int IDENTITY PRIMARY KEY,
    assistant_name varchar(50) NOT NULL,
);

CREATE TABLE Heropower (
    power_id int IDENTITY PRIMARY KEY,
    power_name varchar(50) NOT NULL,
    power_description varchar(200),
);
GO

ALTER TABLE Assistant
	ADD superhero_Id int 
	FOREIGN KEY (superhero_Id) REFERENCES Superhero(superhero_Id);
GO

CREATE TABLE Superhero_Power(
    superhero_id int,
    power_id int, 
	FOREIGN KEY (superhero_id) REFERENCES Superhero(superhero_id),
	FOREIGN KEY (power_id) REFERENCES Heropower(power_id),
    PRIMARY KEY(superhero_id, power_id)
);
GO

INSERT INTO Superhero (superhero_name, superhero_Alias, superhero_Origin) VALUES ('Batman', 'Bruce Wayne', 'Fall into a well with bats');
INSERT INTO Superhero (superhero_name, superhero_Alias, superhero_Origin) VALUES ('Spider-Man', 'Peter Parker', 'Was bitten by a spider'); 
INSERT INTO Superhero (superhero_name, superhero_Alias, superhero_Origin) VALUES ('Superman', 'Clark Kent', 'Came from another planet');  

GO

INSERT INTO Assistant (assistant_name, superhero_id) VALUES ('Bob', 1);
INSERT INTO Assistant (assistant_name, superhero_id) VALUES ('Sven', 1); 
INSERT INTO Assistant (assistant_name) VALUES ('Lisa'); 
GO

INSERT INTO Heropower (power_name, power_description) VALUES ('Money', 'A lot of money');
INSERT INTO Heropower (power_name, power_description) VALUES ('Laser eyes', 'Shoots laser from their eyes'); 
INSERT INTO Heropower (power_name, power_description) VALUES ('Super strength', 'Can lift most things'); 

INSERT INTO Superhero_Power (superhero_id, power_id) VALUES (1, 1)
INSERT INTO Superhero_Power (superhero_id, power_id) VALUES (2, 2)
INSERT INTO Superhero_Power (superhero_id, power_id) VALUES (3, 2)
INSERT INTO Superhero_Power (superhero_id, power_id) VALUES (3, 3)
GO

SELECT * FROM Superhero;


GO


UPDATE Superhero
SET superhero_name = 'Bob'
WHERE superhero_name = 'Batman'
Go


SELECT * FROM Superhero;
Go

SELECT * FROM assistant;

USE SuperheroesDb;
DELETE FROM Assistant
WHERE assistant_name = 'Bob';
Go

SELECT * FROM Assistant;
SELECT * FROM Heropower;
SELECT *  FROM superhero_power;