USE SuperheroesDb;

CREATE TABLE Superhero_Power(
    superhero_id int NOT NULL,
    power_id int NOT NULL, 
	FOREIGN KEY (superhero_id) REFERENCES Superhero(superhero_id),
	FOREIGN KEY (power_id) REFERENCES Heropower(power_id),
    PRIMARY KEY(superhero_id, power_id)
);
GO
