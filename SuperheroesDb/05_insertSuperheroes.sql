USE SuperheroesDb;

INSERT INTO Superhero (superhero_name, superhero_Alias, superhero_Origin) VALUES ('Batman', 'Bruce Wayne', 'Fall into a well with bats');
INSERT INTO Superhero (superhero_name, superhero_Alias, superhero_Origin) VALUES ('Spider-Man', 'Peter Parker', 'Was bitten by a spider'); 
INSERT INTO Superhero (superhero_name, superhero_Alias, superhero_Origin) VALUES ('Superman', 'Clark Kent', 'Came from another planet');  
GO